import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 @IonicPage({
 	name: 'page-register',
 	segment: 'register'
})
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  // public onRegisterForm: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterPage');
  }

  // ngOnInit() {
  //   this.onLoginForm = this._fb.group({
  //     email: ['', Validators.compose([
  //       Validators.required
  //     ])],
  //     password: ['', Validators.compose([
  //       Validators.required
  //     ])]
  //   });
  //
  //   this.onRegisterForm = this._fb.group({
  //     fullName: ['', Validators.compose([
  //       Validators.required
  //     ])],
  //     email: ['', Validators.compose([
  //       Validators.required
  //     ])],
  //     password: ['', Validators.compose([
  //       Validators.required
  //     ])]
  //   });
  // }

  gotoHome() {
    this.navCtrl.setRoot('page-home');
  }

}
