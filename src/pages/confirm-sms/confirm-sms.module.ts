import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ConfirmSmsPage } from './confirm-sms';

@NgModule({
  declarations: [
    ConfirmSmsPage,
  ],
  imports: [
    IonicPageModule.forChild(ConfirmSmsPage),
  ],
	exports: [
		ConfirmSmsPage
	]
})
export class ConfirmSmsPageModule {}
