import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ConfirmSmsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
 @IonicPage({
 	name: 'page-confirm-sms',
 	segment: 'page-confirm-sms'
 })

 @Component({
     selector: 'page-confirm-sms',
     templateUrl: 'confirm-sms.html'
 })
export class ConfirmSmsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ConfirmSmsPage');
  }

  validateSMS() {
    this.navCtrl.setRoot('page-register');
  }

}
