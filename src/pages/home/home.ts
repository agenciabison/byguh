import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, MenuController, ToastController, PopoverController, ModalController } from 'ionic-angular';

import {DishServiceRest} from '../../providers/dish-service-rest';
import {DishService} from '../../providers/dish-service-mock';


@IonicPage({
	name: 'page-home',
	segment: 'home',
	priority: 'high'
})

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})

export class HomePage {

	restaurantopts: String = 'entrada';
	dishes: Array<any>;
  restaurants: Array<any>;
  public bebidas;
	restaurant: any;
  searchKey: string = "";
  yourLocation: string = "463 Beacon Street Guest House";

  constructor(
		public navCtrl: NavController,
		public menuCtrl: MenuController,
		public popoverCtrl: PopoverController,
		public locationCtrl: AlertController,
		public modalCtrl: ModalController,
		public toastCtrl: ToastController,
		public dishService: DishService,
		public dishServiceRest: DishServiceRest) {
		this.menuCtrl.swipeEnable(true, 'authenticated');
		this.menuCtrl.enable(true);
		this.dishes = this.dishService.findAll();
		this.dishServiceRest.getBebidas().subscribe(res => {
			console.log(res);
			this.bebidas = res;
		});

  }
	ionViewDidLoad() {

	}


	openDishDetail(dish) {
		this.navCtrl.push('page-dish-detail', {
			'item': dish
		});
	}
	openBebidaDetail(bebida) {
		this.navCtrl.push('page-dish-detail', {
			'id': bebida.id
		});
	}

  openOrders() {
    this.navCtrl.push('page-orders');
  }

  openCart() {
    this.navCtrl.push('page-cart');
  }

  openSettingsPage() {
  	this.navCtrl.push('page-settings');
  }

	onInput(event) {

	}

	onCancel(event) {
	    alert();
	}


  alertLocation() {
    let changeLocation = this.locationCtrl.create({
      title: 'Change Location',
      message: "Type your Address to change restaurant list in that area.",
      inputs: [
        {
          name: 'location',
          placeholder: 'Enter your new Location',
          type: 'text'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Change',
          handler: data => {
            console.log('Change clicked', data);
            this.yourLocation = data.location;
            let toast = this.toastCtrl.create({
              message: 'Location was change successfully',
              duration: 3000,
              position: 'top',
              closeButtonText: 'OK',
              showCloseButton: true
            });
            toast.present();
          }
        }
      ]
    });
    changeLocation.present();
  }

  presentNotifications(myEvent) {
    console.log(myEvent);
    let popover = this.popoverCtrl.create('page-notifications');
    popover.present({
      ev: myEvent
    });
  }

  ionViewWillEnter() {
      this.navCtrl.canSwipeBack();
  }

}
