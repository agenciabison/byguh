import { Component } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { IonicPage, NavController, MenuController } from 'ionic-angular';

@IonicPage({
	name: 'page-auth',
	segment: 'auth',
	priority: 'high'
})

@Component({
  selector: 'page-auth',
  templateUrl: 'auth.html'
})
export class AuthPage {

	  public fieldPhone: boolean = false;

  constructor(private _fb: FormBuilder, public nav: NavController, public menu: MenuController) {
		this.menu.swipeEnable(false);
		this.menu.enable(false);
  }

  login() {
    this.fieldPhone = !this.fieldPhone;
  }

  loginFinal() {
    this.nav.setRoot('page-confirm-sms');
  }


}
