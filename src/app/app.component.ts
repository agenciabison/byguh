import {Component, ViewChild} from '@angular/core';
import {Nav, Platform} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

export interface MenuItem {
    title: string;
    component: any;
    icon: string;
}

@Component({
    templateUrl: 'app.html'
})
export class foodIonicApp {
    @ViewChild(Nav) nav: Nav;

  	tabsPlacement: string = 'bottom';
  	tabsLayout: string = 'icon-top';

    rootPage: any = 'page-auth';
    showMenu: boolean = true;
    homeItem: any;
    initialItem: any;
    messagesItem: any;
    settingsItem: any;
    appMenuItems: Array<MenuItem>;
    accountMenuItems: Array<MenuItem>;
    helpMenuItems: Array<MenuItem>;

    constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen) {
        this.initializeApp();

        this.homeItem = { component: 'page-home' };
        this.messagesItem = { component: 'page-message-list'};

        this.appMenuItems = [
            {title: 'Cardápio', component: 'page-home', icon: 'map'},
            // {title: 'Favoritos', component: 'page-favorite-dish', icon: 'heart'},
            {title: 'Pedidos', component: 'page-orders', icon: 'list-box'},
            {title: 'Promoções', component: 'page-dish-list', icon: 'pizza'},
            {title: 'Carrinho', component: 'page-cart', icon: 'cart'}
        ];

        this.accountMenuItems = [
            {title: 'Login', component: 'page-auth', icon: 'log-in'},
            {title: 'Minha conta', component: 'page-my-account', icon: 'contact'},
            {title: 'Logout', component: 'page-auth', icon: 'log-out'},
        ];

        this.helpMenuItems = [
            {title: 'O By Guh', component: 'page-about', icon: 'information-circle'},
            // {title: 'Suporte', component: 'page-support', icon: 'call'},
            {title: 'Configurações', component: 'page-settings', icon: 'cog'},
            {title: 'Login', component: 'page-auth', icon: 'photos'}
        ];
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.overlaysWebView(false);
            this.splashScreen.hide();
        });

	    if (!this.platform.is('mobile')) {
	      this.tabsPlacement = 'top';
	      this.tabsLayout = 'icon-left';
	    }
    }

    openPage(page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    }
}
