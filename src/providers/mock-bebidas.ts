let bebidas: Array<any> = [
    {
        id: 1,
        name: "Vinho Branco",
        description: "Com um estilo jovem e descomplicado, esse branco expressa que Bordeaux também produz vinhos modernos, mesmo com toda sua tradição.",
		picture: "https://images-submarino.b2w.io/produtos/01/00/item/112177/0/112177011SZ.jpg",
		images: [
			"https://images-submarino.b2w.io/produtos/01/00/item/112177/0/112177011SZ.jpg"
		],
        price: 39.90,
        qtd: 0
    },
    {
        id: 2,
        name: "Coca Cola",
        description: "Garrafa 2 litros",
		picture: "https://target.scene7.com/is/image/Target/GUEST_f3dfc07b-ecf3-4db4-bb56-a1be8b754215?wid=488&hei=488&fmt=pjpeg",
		images: [
			"https://target.scene7.com/is/image/Target/GUEST_f3dfc07b-ecf3-4db4-bb56-a1be8b754215?wid=488&hei=488&fmt=pjpeg"
		],
        price: 8.90,
        qtd: 0
    },
    {
        id: 3,
        name: "Suco Natural",
        description: "Na linha refrigerada você encontra os ingredientes que precisam do friozinho da geladeira para manter o máximo do frescor, sabor e nutrientes do nosso suco 100% natural.",
		picture: "https://http2.mlstatic.com/extrator-fruta-sucos-natural-profissional-espremedo-de-suco-D_NQ_NP_983525-MLB25459284738_032017-F.jpg",
		images: [
			"https://http2.mlstatic.com/extrator-fruta-sucos-natural-profissional-espremedo-de-suco-D_NQ_NP_983525-MLB25459284738_032017-F.jpg"
		],
        price: 7.90,
        qtd: 0
    },
    {
        id: 4,
        name: "Vinho Tinto",
        description: "Mais do que um vinho, uma companhia para qualquer momento! Além das refeições, vai bem com um momento relax após o trabalho, um livro, ou mesmo para curtir quem você mais gosta.",
		picture: "https://emporiodacerveja.vteximg.com.br/arquivos/ids/162784-500-500/vinho-tinto-frances-bordeaux-haut-mouleyres.jpg",
		images: [
			"https://emporiodacerveja.vteximg.com.br/arquivos/ids/162784-500-500/vinho-tinto-frances-bordeaux-haut-mouleyres.jpg"
		],
        price: 7.75,
        qtd: 0
    },
    {
        id: 5,
        name: "Vinho Branco",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
    picture: "https://images-submarino.b2w.io/produtos/01/00/item/112177/0/112177011SZ.jpg",
    images: [
      "https://images-submarino.b2w.io/produtos/01/00/item/112177/0/112177011SZ.jpg"
    ],
        price: 39.90,
        qtd: 0
    },
    {
        id: 6,
        name: "Coca Cola",
        description: "Garrafa 2 litros",
    picture: "https://target.scene7.com/is/image/Target/GUEST_f3dfc07b-ecf3-4db4-bb56-a1be8b754215?wid=488&hei=488&fmt=pjpeg",
    images: [
      "https://target.scene7.com/is/image/Target/GUEST_f3dfc07b-ecf3-4db4-bb56-a1be8b754215?wid=488&hei=488&fmt=pjpeg"
    ],
        price: 8.90,
        qtd: 0
    },
    {
        id: 7,
        name: "Suco Natural",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
    picture: "https://http2.mlstatic.com/extrator-fruta-sucos-natural-profissional-espremedo-de-suco-D_NQ_NP_983525-MLB25459284738_032017-F.jpg",
    images: [
      "https://http2.mlstatic.com/extrator-fruta-sucos-natural-profissional-espremedo-de-suco-D_NQ_NP_983525-MLB25459284738_032017-F.jpg"
    ],
        price: 7.90,
        qtd: 0
    },
    {
        id: 8,
        name: "Vinho Tinto",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus at consequuntur dolores, ea reprehenderit ipsam voluptas nulla recusandae.",
    picture: "https://emporiodacerveja.vteximg.com.br/arquivos/ids/162784-500-500/vinho-tinto-frances-bordeaux-haut-mouleyres.jpg",
    images: [
      "https://emporiodacerveja.vteximg.com.br/arquivos/ids/162784-500-500/vinho-tinto-frances-bordeaux-haut-mouleyres.jpg"
    ],
        price: 7.75,
        qtd: 0
    }


];

export default bebidas;
