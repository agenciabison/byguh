import { Injectable } from '@angular/core';
// import { api } from './config';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map, catchError } from 'rxjs/operators';

// let dishesURL = api.SERVER_URL + 'dishes/';

@Injectable()
export class DishServiceRest {

  constructor(public http: HttpClient) {

  }

  findAll() {
      // return this.dishes;
  }

  // Bebidas API
  public getBebidas(): Observable<{}> {
    return this.http.get("http://35.238.203.204/api/products").pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  private handleError (error: Response | any) {
    let errMsg: string;
    if (error instanceof Response) {
      const err = error || '';
      errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
      errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return Observable.throw(errMsg);
  }

}
